#!/bin/bash


error::log() {
    local code=${1:-}
    local message=${2:-}
    local stack=${3:-}
    printf "%s\n" "Error${code:+[${code}]}${message:+: ${message}}" >&2
    if [ -n "${stack}" ] ; then
        printf "%s\n" "${stack}" >&2
    fi
}

error::get_stack() { 
    local offset=${1:-1}; 
    local NL=$'\n'; 
    local STACK; 
    local depth=${#FUNCNAME[@]}; 
    local d=${offset}; 
    local d_prev=$((d-1))
    local lineno
    
    while [[ ${d} -lt "${depth}" ]] ; do
        lineno=${BASH_LINENO[d_prev]}
        STACK="${STACK}    at ${BASH_SOURCE[d]}#${FUNCNAME[d]}:${lineno}${NL}"; 
        d_prev=${d}
        d=$((d+1)); 
   done; 
   _r="${STACK%${NL}*}";  
}

error::throw() { 
    local code=${1:-}; 
    local message=${2:-}; 
    error::get_stack 2; 
    error::log "${code}" "${message}" "${_r}"
}

throw() {
    error::throw "${1}" "${2}"
}
